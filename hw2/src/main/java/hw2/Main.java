package hw2;

import hw2.game.TicTacToe;

public class Main {

    public static void main(String[] args) {
        TicTacToe ticTacToe = new TicTacToe();
        ticTacToe.startPersonVsComputerGame();
    }
}
