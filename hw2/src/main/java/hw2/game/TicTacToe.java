package hw2.game;

import hw2.game.model.CellSign;
import hw2.game.model.GameModel;
import hw2.game.model.player.Computer;
import hw2.game.model.player.Person;
import hw2.game.model.player.Player;

public class TicTacToe {

    public void startPersonVsComputerGame() {
        GameModel model = new GameModel(CLIUtils.getGameFieldSize());

        Person person = new Person(CLIUtils.getPersonPreferredSign(), model);
        Computer computer = new Computer(person.getCellSign().getOppositeSign(), model);

        Player player1 = person.getCellSign() == CellSign.X
            ? person
            : computer;
        Player player2 = person.getCellSign() == CellSign.X
            ? computer
            : person;

        CLIUtils.printGameField(model);
        while (true) {
            model.setFieldCellSign(player1.getCellSign(), player1.getNextMoveCoordinates());

            CLIUtils.printMessage("Ход 1-го игрока:");
            CLIUtils.printGameField(model);
            if (model.isGameOver()) {
                if (model.isDraw()) {
                    CLIUtils.printMessage("Ничья!!!");
                } else {
                    CLIUtils.printMessage(player1.getPlayerName() + " победил!!!");
                }
                break;
            }

            model.setFieldCellSign(player2.getCellSign(), player2.getNextMoveCoordinates());

            CLIUtils.printMessage("Ход 2-го игрока:");
            CLIUtils.printGameField(model);
            if (model.isGameOver()) {
                if (model.isDraw()) {
                    CLIUtils.printMessage("Ничья!!!");
                } else {
                    CLIUtils.printMessage(player2.getPlayerName() + " победил!!!");
                }
                break;
            }
        }
        CLIUtils.printMessage("Конец игры");
    }
}
