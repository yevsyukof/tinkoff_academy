package hw2.game;

import hw2.game.model.CellSign;
import hw2.game.model.GameConst;
import hw2.game.model.GameModel;
import hw2.utils.Pair;
import java.util.Scanner;

public class CLIUtils {

    private CLIUtils(){}

    private static final Scanner scanner = new Scanner(System.in);

    public static int getGameFieldSize() {
        while (true) {
            System.out.print("Введите размер игрового поля: ");
            int gameFieldSize = scanner.nextInt();

            if (gameFieldSize < GameConst.MIN_GAME_FIELD_SIZE) {
                System.out.println("Размер поля не может быть меньше 3");
                continue;
            }
            return gameFieldSize;
        }
    }

    public static CellSign getPersonPreferredSign() {
        System.out.println("Выберете за какую сторону играть");
        System.out.print("Введите \"1\" для игры за 'X' и \"2\" для игры за 'O': ");

        while (true) {
            int sideNum = scanner.nextInt();
            switch (sideNum) {
                case 1 -> {
                    return CellSign.X;
                }
                case 2 -> {
                    return CellSign.O;
                }
                default -> System.out.print("Введите 1 или 2: ");
            }
        }
    }

    public static void printMessage(String message) {
        System.out.println(message);
    }

    // Считываем из терминала координаты следующего хода игрока
    public static Pair<Integer, Integer> readNextMoveCoordinates() {
        System.out.print("Введите координаты следущего хода (в формате <1...N 1...N>): ");
        return new Pair<>(
            scanner.nextInt() - GameConst.INPUT_COORDINATES_OFFSET,
            scanner.nextInt() - GameConst.INPUT_COORDINATES_OFFSET);
    }

    public static void printGameField(GameModel model) {
        System.out.println(model.getGameFieldStringRepresentation());
        System.out.println();
    }
}
