package hw2.game.model;

// Возможные состояния клеток игрового поля
public enum CellSign {
    X, O, EMPTY;

    // Возвращаем символ противоположной стороны в игре
    public CellSign getOppositeSign() {
        return switch (this) {
            case X -> O;
            case O -> X;
            case EMPTY -> EMPTY;
        };
    }

    public char getCharValue() {
        return switch (this) {
            case X -> 'X';
            case O -> 'O';
            case EMPTY -> '.';
        };
    }
}
