package hw2.game.model;

import hw2.utils.Pair;

public class GameModel {

    private final GameField gameField;

    public GameModel(int gameFieldSize) {
        this.gameField = new GameField(gameFieldSize);
    }

    public boolean isNotEmptyCellAt(Pair<Integer, Integer> coordinates) {
        return gameField.getCellSignAt(coordinates.getLeft(), coordinates.getRight())
            != CellSign.EMPTY;
    }

    // Метод проверяет валидность введенной пары координат игрового поля
    public boolean isNotValidCellCoordinates(Pair<Integer, Integer> coordinates) {
        return coordinates.getLeft() < 0 || coordinates.getLeft() >= gameField.getSize()
            || coordinates.getRight() < 0 && coordinates.getRight() >= gameField.getSize();
    }

    public boolean isGameOver() {
        return isDraw() || isWin();
    }

    // Проверка того, что одна из сторон смогла полностью заполнить какую-либо "выигрышную линию"
    public boolean isWin() {
        for (int diagonalCellIndex = 0; diagonalCellIndex < gameField.getSize(); diagonalCellIndex++) {
            if (gameField.isWinAt(diagonalCellIndex, diagonalCellIndex)) {
                return true;
            }
        }
        return false;
    }

    public boolean isDraw() {
        return gameField.isFull();
    }

    public String getGameFieldStringRepresentation() {
        return gameField.toString();
    }

    public void setFieldCellSign(CellSign playerSign, Pair<Integer, Integer> coordinates) {
        gameField.setFieldCellSign(playerSign, coordinates.getLeft(), coordinates.getRight());
    }

    public void setFieldCellEmpty(Pair<Integer, Integer> coordinates) {
        gameField.setCellEmptyAt(coordinates.getLeft(), coordinates.getRight());
    }

    public GameField getGameField() {
        return gameField;
    }
}
