package hw2.game.model;

import hw2.utils.Pair;

public class MinimaxCalculator {

    private static final int MAX_DEPTH = 5;

    private static final int WIN_SCORES = 10;
    private static final int DEFEAT_SCORES = -10;
    private static final int DRAW_SCORES = 0;

    private final CellSign calculatorUserCellSign;

    public MinimaxCalculator(CellSign calculatorUserCellSign) {
        this.calculatorUserCellSign = calculatorUserCellSign;
    }

    public Pair<Integer, Integer> getBestNextMoveCoordinates(GameModel model) {

        Pair<Integer, Integer> curBestCoordinates = null;
        int curBestMoveCost = Integer.MIN_VALUE;

        for (int i = 0; i < model.getGameField().getSize(); i++) {
            for (int j = 0; j < model.getGameField().getSize(); j++) {
                if (model.getGameField().getCellSignAt(i, j) == CellSign.EMPTY) {
                    var curMoveCoordinates = new Pair<>(i, j);
                    int curMoveCost = calcScoresForMove(
                        0, calculatorUserCellSign, curMoveCoordinates, model);

                    if (curMoveCost > curBestMoveCost) {
                        curBestMoveCost = curMoveCost;
                        curBestCoordinates = curMoveCoordinates;
                    }
                }
            }
        }
        return curBestCoordinates;
    }

    private int scores(int depth, CellSign lastMoveCellSign, GameModel model) {
        if (model.isWin()) {
            return lastMoveCellSign == calculatorUserCellSign
                ? WIN_SCORES - depth
                : depth - DEFEAT_SCORES;
        }
        return DRAW_SCORES;
    }

    private int calcScoresForMove(
        int depth, CellSign moveSign, Pair<Integer, Integer> moveCoordinates, GameModel model) {

        model.setFieldCellSign(moveSign, moveCoordinates);

        if (model.isGameOver() || depth > MAX_DEPTH) {
            int scores = scores(depth, moveSign, model);
            model.setFieldCellEmpty(moveCoordinates);
            return scores;
        }

        int bestMoveCost = moveSign == calculatorUserCellSign
            ? Integer.MIN_VALUE
            : Integer.MAX_VALUE;

        for (int i = 0; i < model.getGameField().getSize(); i++) {
            for (int j = 0; j < model.getGameField().getSize(); j++) {
                if (model.getGameField().getCellSignAt(i, j) == CellSign.EMPTY) {
                    int curMoveCost = calcScoresForMove(
                        depth + 1, moveSign.getOppositeSign(), new Pair<>(i, j), model);

                    if (moveSign == calculatorUserCellSign && curMoveCost > bestMoveCost) {
                        bestMoveCost = curMoveCost;
                    } else if (moveSign == calculatorUserCellSign.getOppositeSign()
                        && curMoveCost < bestMoveCost) {
                        bestMoveCost = curMoveCost;
                    }
                }
            }
        }
        model.setFieldCellEmpty(moveCoordinates);
        return bestMoveCost;
    }
}
