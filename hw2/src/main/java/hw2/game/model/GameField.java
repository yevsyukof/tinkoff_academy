package hw2.game.model;

import java.util.Arrays;

public class GameField {

    private final char[][] gameField;
    private final int fieldSize;

    private int numEmptyCells;

    public GameField(int fieldSize) {
        this.fieldSize = fieldSize;
        gameField = new char[fieldSize][fieldSize];
        numEmptyCells = fieldSize * fieldSize;

        for (char[] fieldLine : gameField) {
            Arrays.fill(fieldLine, CellSign.EMPTY.getCharValue());
        }
    }

    public void setFieldCellSign(CellSign cellSign, int lineIdx, int columnIdx) {
        gameField[lineIdx][columnIdx] = cellSign.getCharValue();
        numEmptyCells--;
    }

    public void setCellEmptyAt(int lineIdx, int columnIdx) {
        gameField[lineIdx][columnIdx] = CellSign.EMPTY.getCharValue();
        numEmptyCells++;
    }

    public int getSize() {
        return fieldSize;
    }

    public boolean isFull() {
        return numEmptyCells == 0;
    }

    public CellSign getCellSignAt(int lineIdx, int columnIdx) {
        return switch (gameField[lineIdx][columnIdx]) {
            case 'X' -> CellSign.X;
            case 'O' -> CellSign.O;
            default -> CellSign.EMPTY;
        };
    }

    /**
    * Проверяем, является ли клетка с переданными координатами частью какой-либо "выигрышной линии"
    */
    public boolean isWinAt(int lineIdx, int columnIdx) {
        return gameField[lineIdx][columnIdx] != CellSign.EMPTY.getCharValue()
            && (checkVertical(lineIdx, columnIdx)
            || checkHorizontal(lineIdx, columnIdx)
            || checkStraightDiagonal(lineIdx, columnIdx)
            || checkReverseDiagonal(lineIdx, columnIdx));
    }

    // Проверяем что все символы на одной вертикали являются одинаковыми
    private boolean checkVertical(int lineIdx, int columnIdx) {
        for (int i = 0; i < fieldSize; i++) {
            if (gameField[i][columnIdx] != gameField[lineIdx][columnIdx]) {
                return false;
            }
        }
        return true;
    }

    // Проверяем что все символы на одной горизонтали являются одинаковыми
    private boolean checkHorizontal(int lineIdx, int columnIdx) {
        for (int i = 0; i < fieldSize; i++) {
            if (gameField[lineIdx][i] != gameField[lineIdx][columnIdx]) {
                return false;
            }
        }
        return true;
    }

    // Проверяем что все символы на прямой диагонали являются одинаковыми
    private boolean checkStraightDiagonal(int lineIdx, int columnIdx) {
        if (lineIdx != columnIdx) {
            return false;
        }

        for (int i = 0; i < fieldSize; i++) {
            if (gameField[i][i] != gameField[lineIdx][columnIdx]) {
                return false;
            }
        }
        return true;
    }

    // Проверяем что все символы на обратной диагонали являются одинаковыми
    private boolean checkReverseDiagonal(int lineIdx, int columnIdx) {
        if (lineIdx != fieldSize - 1 - columnIdx) {
            return false;
        }

        for (int i = 0; i < fieldSize; i++) {
            if (gameField[i][fieldSize - 1 - i] != gameField[lineIdx][columnIdx]) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (var line : gameField) {
            sb.append(line);
            sb.append('\n');
        }
        return sb.toString();
    }
}
