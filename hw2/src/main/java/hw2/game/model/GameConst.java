package hw2.game.model;

public class GameConst {

    public static final int MIN_GAME_FIELD_SIZE = 3;

    public static final int INPUT_COORDINATES_OFFSET = 1;

}
