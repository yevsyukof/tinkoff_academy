package hw2.game.model.player;

import hw2.game.CLIUtils;
import hw2.game.model.CellSign;
import hw2.game.model.GameModel;
import hw2.utils.Pair;

public class Person extends Player {

    public Person(CellSign preferredCellSign, GameModel gameModel) {
        super(preferredCellSign,gameModel);
    }

    @Override
    public Pair<Integer, Integer> getNextMoveCoordinates() {
        Pair<Integer, Integer> nextMoveCoordinates;
        do {
            nextMoveCoordinates = CLIUtils.readNextMoveCoordinates();
        } while (isNotValidNextMoveCoordinates(nextMoveCoordinates));
        return nextMoveCoordinates;
    }

    private boolean isNotValidNextMoveCoordinates(Pair<Integer, Integer> nextMoveCoordinates) {
        return gameModel.isNotValidCellCoordinates(nextMoveCoordinates)
            || gameModel.isNotEmptyCellAt(nextMoveCoordinates);
    }

    @Override
    public String getPlayerName() {
        return "Игрок";
    }
}
