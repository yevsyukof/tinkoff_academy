package hw2.game.model.player;


import hw2.game.model.CellSign;
import hw2.game.model.GameModel;
import hw2.utils.Pair;

public abstract class Player {

    protected final CellSign playerCellSign;
    protected final GameModel gameModel;

    protected Player(CellSign playerCellSign, GameModel gameModel) {
        this.playerCellSign = playerCellSign;
        this.gameModel = gameModel;
    }

    public abstract Pair<Integer, Integer> getNextMoveCoordinates();

    public CellSign getCellSign() {
        return playerCellSign;
    }

    public abstract String getPlayerName();
}
