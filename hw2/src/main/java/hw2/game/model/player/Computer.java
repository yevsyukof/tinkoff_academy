package hw2.game.model.player;

import hw2.game.model.CellSign;
import hw2.game.model.GameModel;
import hw2.game.model.MinimaxCalculator;
import hw2.utils.Pair;

public class Computer extends Player {

    private final MinimaxCalculator minimaxCalculator;

    public Computer(CellSign playerCellSign, GameModel gameModel) {
        super(playerCellSign, gameModel);
        this.minimaxCalculator = new MinimaxCalculator(playerCellSign);
    }

    // Расчитываем наилучший следующий ход в сложившейся ситуации, используя алгоритм Минимакс
    @Override
    public Pair<Integer, Integer> getNextMoveCoordinates() {
        return minimaxCalculator.getBestNextMoveCoordinates(gameModel);
    }

    @Override
    public String getPlayerName() {
        return "Компьютер";
    }
}
