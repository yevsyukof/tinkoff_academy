package academy.hw1.task4;

import java.util.function.Supplier;

public class SupplierTask<T> implements Runnable {

    private static final int SUPPLY_DELAY_SEC = 5;
    private static final int MILLISECONDS_PER_SECOND = 1000;

    private final Store<T> store;
    private final Supplier<? extends T> supplier;

    public SupplierTask(Store<T> store, Supplier<? extends T> supplier) {
        this.store = store;
        this.supplier = supplier;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                store.putItem(supplier.get());

                Thread.sleep(SUPPLY_DELAY_SEC * MILLISECONDS_PER_SECOND);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}