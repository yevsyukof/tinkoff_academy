package academy.hw1.task4;

public class ConsumerTask<T> implements Runnable {

    private static final int CONSUMER_DELAY_SEC = 2;
    private static final int MILLISECONDS_PER_SECOND = 1000;

    private final Store<T> store;

    public ConsumerTask(Store<T> store) {
        this.store = store;
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                store.getItem();

                Thread.sleep(CONSUMER_DELAY_SEC * MILLISECONDS_PER_SECOND);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }
    }
}
