package academy.hw1;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task2 {

    public Map<Integer, Integer> listToRepeatedNumbersStatMap(List<Integer> numbers) {
        return numbers.stream()
            .collect(Collectors.toMap(Function.identity(), num -> 1, Integer::sum))
            .entrySet().stream()
            .filter(e -> e.getValue() > 1)
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }
}
