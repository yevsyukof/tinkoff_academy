package hw4.streams;

import hw4.seminar.User;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Task2 {
    private final Set<User> users = new HashSet<>();

    public int getTotalAge() {
        // заменил начальную логику на работу со специальным стримом для примитивов
        return users.stream().mapToInt(User::getAge).sum();
    }

    // заменил создания единого стрима со всеми юзерами (через сливание стримов юзеров), у которого
    // мы брали размер, на создание специального стрим длин листов
    public int countEmployees(Map<String, List<User>> departments) {
        return (int) departments.values().stream()
                .mapToLong(List::size)
                .sum();
    }
}
