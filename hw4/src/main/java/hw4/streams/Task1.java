package hw4.streams;

import hw4.seminar.User;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

public class Task1 {

    // была написана страшная, нечитаемая работа с несколькими стримами
    public Map<String, Integer> getMaxAgeByUserName(List<User> users) {

        // Вынес сохранение промежуточного состояния в локальную переменную
        Map<String, List<User>> usersByUserName = users.stream().collect(groupingBy(User::getName));
        return usersByUserName.entrySet().stream()
            .collect(toMap(Map.Entry::getKey, entry -> findUsersMaxAge(entry.getValue())));
    }

    // написал метод поиска максимального возраста у юзеров
    private int findUsersMaxAge(List<User> users) {
        return users.stream()
            .mapToInt(User::getAge)
            .max()
            .orElse(0);
    }
}
