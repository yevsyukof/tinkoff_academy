package hw4.streams;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.IntStream;

public class Task3 {
    public void printNameStats(List<User> users) {
        // убрал повторный вызов getNameLengthStream(), который создает дополнительные расходы
        IntSummaryStatistics summaryStatistics = getNameLengthStream(users).summaryStatistics();
        if (summaryStatistics.getCount() > 0) {
            System.out.println("MAX: " + summaryStatistics.getMax());
            System.out.println("MIN: " + summaryStatistics.getMin());
        }
    }

    private IntStream getNameLengthStream(List<User> users) {
        return users.stream()
            .mapToInt(user -> user.getName().length());
    }
}
