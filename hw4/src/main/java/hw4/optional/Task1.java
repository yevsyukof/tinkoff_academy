package hw4.optional;


import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Task1 {


    public List<Email> create() {
        // убрал 3-ий входной параметр (null)
        Email noAttachment = new Email("First!", "No attachment");
        Attachment attachment = new Attachment("/mnt/files/image.png", 370);
        Email withAttachment = new Email("Second!", "With attachment", attachment);
        return Arrays.asList(noAttachment, withAttachment);
    }

    /* У это класса было Optional свойство,
    *  но при этом класс имплиментирует Serializable */
    class Email implements Serializable {
        private final String subject;
        private final String body;

        // Убрал Optional у свойства класса
        private final Attachment attachment;

        // Изменил входной параметр конструктора, убрав Optional у Attachment
        Email(String subject, String body, Attachment attachment) {
            this.subject = subject;
            this.body = body;
            this.attachment = attachment;
        }

        // Добавил конструктор без Attachment
        Email(String subject, String body) {
            this.subject = subject;
            this.body = body;
            this.attachment = null;
        }

        String getSubject() {
            return subject;
        }

        String getBody() {
            return body;
        }

        // Изменил геттер для атачмента
        Optional<Attachment> getAttachment() {
            return Optional.ofNullable(attachment);
        }
    }



    class Attachment {
        private final String path;
        private final int size;

        Attachment(String path, int size) {
            this.path = path;
            this.size = size;
        }

        String getPath() {
            return path;
        }

        int getSize() {
            return size;
        }
    }
}
