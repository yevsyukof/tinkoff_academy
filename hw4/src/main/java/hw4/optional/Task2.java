package hw4.optional;

import java.util.List;
import java.util.Optional;

public class Task2 {

    private static final String ADMIN_ROLE = "admin";

    public Optional<User> findAnyAdmin() {
        Optional<List<User>> users = findUsersByRole(ADMIN_ROLE);

        // Была небольшая портяночка с каскадным раскрытием Optional-а в условном выражении
        // Заменил все это условие через filter() и orElseThrow()
        return users.filter(list -> !list.isEmpty()).map(list -> list.get(0));
    }

    private Optional<List<User>> findUsersByRole(String role) {
        //real search in DB
        return Optional.empty();
    }

}
