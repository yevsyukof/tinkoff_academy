package hw4.lambdas;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

public class Task2 {
    private final Map<String, Set<User>> usersByRole = new HashMap<>();

    /*
    * В этом задании используется очень длинная лямбда
    * */
    public void addUser(User user) {

        // Вынес тело лямбды в отдельный метод для более простой читаймости
        user.getRoles().forEach(role -> addUserByRole(role, user));
    }

    // Переписанное и упрощенное тело лямбды
    private void addUserByRole(Role role, User user) {
        Set<User> usersInRole = usersByRole.computeIfAbsent(role.getName(), missingRoleName -> new HashSet<>());
        usersInRole.add(user);
        usersByRole.putIfAbsent(role.getName(), usersInRole);
    }

    public Set<User> getUsersInRole(String roleName) {
        return usersByRole.getOrDefault(roleName, Collections.emptySet());
    }

}
