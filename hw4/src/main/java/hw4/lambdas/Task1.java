package hw4.lambdas;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Task1 {
    private final Set<User> users = new HashSet<>();

    /*
    * В данном задании, мне кажется, используется сложная лямбда с плохим названием
    * входного параметра, который непонятно какого типа
    * */
    public void removeUsersWithPermission(Permission permission) {
        Iterator<User> iterator = users.iterator();

        while (iterator.hasNext()) {
            User user = iterator.next();

            /* Я вынес это большое условие в отдельный метод
            *  И чтобы в предикате было понятно с каким типом данных мы работаем,
            *  я замапил и слил все это в один стрим пермишенов юзера
            * */
            if (isUserHavePermission(user, permission)) {
                iterator.remove();
            }
        }
    }

    private boolean isUserHavePermission(User user, Permission requiredPermission) {
        return user.getRoles().stream()
            .map(Role::getPermissions)
            .flatMap(Set::stream)
            .anyMatch(permission -> permission.equals(requiredPermission));
    }
}